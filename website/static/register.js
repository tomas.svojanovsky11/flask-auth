const button = document.querySelector(".d-grid .btn");
const email = document.querySelector("#email");
const firstName = document.querySelector("#last-name");
const lastName = document.querySelector("#first-name");
const password = document.querySelector("#password");
const confirmPassword = document.querySelector("#confirm-password");

let emailValue = email.value;
let firstNameValue = firstName.value;
let lastNameValue = lastName.value;
let passwordValue = password.value;
let confirmPasswordValue = confirmPassword.value;

button.setAttribute("disabled", "");

function isAllEmpty() {
console.log(emailValue, firstNameValue, lastNameValue, passwordValue, confirmPasswordValue);
    return [emailValue, firstNameValue, lastNameValue, passwordValue, confirmPasswordValue].every(Boolean);
}

function setButtonState() {
    const isOk = isAllEmpty();
    if (isOk) {
        button.removeAttribute("disabled");
    } else {
        button.setAttribute("disabled", "");
    }
}

email.addEventListener("keydown", (e) => {
    const { value } = e.target;
    lastNameValue = value;
    setButtonState();
});

firstName.addEventListener("keydown", (e) => {
    const { value } = e.target;
    firstNameValue = value;
    setButtonState();
});

lastName.addEventListener("keydown", (e) => {
    const { value } = e.target;
    lastNameValue = value;
    setButtonState();
});

password.addEventListener("keydown", (e) => {
    const { value } = e.target;
    passwordValue = value;
    setButtonState();
});

confirmPassword.addEventListener("keydown", (e) => {
    const { value } = e.target;
    confirmPasswordValue = value;
    setButtonState();
});